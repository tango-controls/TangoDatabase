#ifndef DATABASECONNECTIONPOOL_H
#define DATABASECONNECTIONPOOL_H

#include <memory>
#include "common.h"

class DatabaseConnectionPool;
using DatabaseConnectionPoolPtr = std::shared_ptr<DatabaseConnectionPool>;

/// Management class for the database connection pool
class DatabaseConnectionPool : public Tango::LogAdapter
{
public:
  DatabaseConnectionPool(Tango::DeviceImpl *dev,
                         const char* mysql_user,
                         const char* mysql_password,
                         const char* mysql_host,
                         const char* mysql_db_name);

  ~DatabaseConnectionPool();

  DatabaseConnectionPool(const DatabaseConnectionPool&) = delete;
  DatabaseConnectionPool& operator=(const DatabaseConnectionPool&) = delete;
  DatabaseConnectionPool(DatabaseConnectionPool&&) = delete;
  DatabaseConnectionPool& operator=(DatabaseConnectionPool&&) = delete;

  /// Only user: DatabaseConnectionHandle
  /// @{
  MYSQL* GetDatabase(int con_nb);
  int get_connection();
  void release_connection(int con_nb);
  /// @}

  /// Set the connection pool size from main() before the DS is created
	static void set_conn_pool_size(int si);

private:
	static int conn_pool_size;
  int m_last_sem_wait;
	unsigned long	m_mysql_svr_version;
  struct DbConnection;
  DbConnection *m_conn_pool;
	omni_mutex m_sem_wait_mutex;

  void base_connect(int loop);
  void create_connection_pool(const char* mysql_user,
                              const char* mysql_password,
                              const char* mysql_host,
                              const char* mysql_db_name);
};

#endif // DATABASECONNECTIONPOOL_H
