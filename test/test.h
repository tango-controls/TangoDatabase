#pragma once

#define BOOST_TEST_MODULE TangoDataBaseTests
#include <boost/test/unit_test.hpp>

#include <tango/tango.h>

#include "boost_test_helper.h"
